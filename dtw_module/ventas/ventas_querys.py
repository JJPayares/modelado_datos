import pandas as pd

from dtw_module.sql_generic_methods import *

from utils.basic_utils import *

data_file_path = "../../files/datos/FU ORDER DETAIL - testcube.csv"
data_file = pd.read_csv(data_file_path)


def insert_ventas():
    # build the query and params
    query = "INSERT INTO ventas values (%(id_venta)s, %(id_cliente)s, %(id_vendedor)s, %(id_medio_pago)s, " \
            "%(fecha)s, %(sub_total)s, %(iva)s);"

    vendedores = get_first_element_of_element(
        get_results_by_param_and_table('id_empleado', 'id_tipo_empleado', 1, 'empleado', '=')
    )
    clientes = get_first_element_of_element(
        get_results_by_param_and_table('id_cliente', 'id_cliente', 0, 'cliente', '<>')
    )
    for index, row in data_file.iterrows():
        vendedor = random.choice(vendedores)
        cliente = random.choice(clientes)
        medio_pago = random_int(1, 7)
        fecha = random_date()
        iva = 0.19
        values = {'id_venta': index + 1, 'id_cliente': cliente, 'id_vendedor': vendedor, 'id_medio_pago': medio_pago,
                  'fecha': fecha, 'sub_total': row["payment_amount"], 'iva': iva}
        try:
            generic_insert_whit_dict(query, values)
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)


insert_ventas()
