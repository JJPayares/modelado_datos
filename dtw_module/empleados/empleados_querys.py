import pandas as pd

from dtw_module.sql_generic_methods import *
from utils.basic_utils import *

empleados_file_path = "../../files/empleado.csv"
empleados_file = pd.read_csv(empleados_file_path)


def insert_empleados():
    # build the query and params
    query = "INSERT INTO empleado values " \
            "( %(id_empleado)s, %(nombre)s, %(apellido)s, %(edad)s, %(id_tipo_doc)s, %(email)s," \
            " %(id_sucursal)s, %(id_tipo_empleado)s);"

    for index, row in empleados_file.iterrows():
        edad = random_int(18, 67)
        id_tipo_documento = random_int(1, 7)
        sucursal = random_int(1, 50)
        id_tipo_empleado = random_int(1, 7)

        values = {'id_empleado': index + 1, 'nombre': row["Nombre"], 'apellido': row["Apellido"],
                  'edad': edad, 'id_tipo_doc': id_tipo_documento, 'email': row["Email"], 'id_sucursal': sucursal,
                  'id_tipo_empleado': id_tipo_empleado}
        try:
            generic_insert_whit_dict(query, values)
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)


insert_empleados()
