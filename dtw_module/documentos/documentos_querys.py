import pandas as pd

from dtw_module.sql_generic_methods import *

document_types_file_path = "../../files/TIPOS DE DOCUMENTO.csv"
document_types_file = pd.read_csv(document_types_file_path)


def insert_documents_type_from_file():
    # build the query and params
    query = "INSERT INTO tipo_documento values (%(id_tipo_doc)s, %(abrev_tipo_doc)s, %(tipo_doc)s)"

    for index, row in document_types_file.iterrows():
        values = {'id_tipo_doc': index + 1, 'abrev_tipo_doc': row["ABREVIATURA"], 'tipo_doc': row["TIPO DE DOCUMENTO"]}
        try:
            generic_insert_whit_dict(query, values)
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)


insert_documents_type_from_file()
