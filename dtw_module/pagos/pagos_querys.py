import pandas as pd

from dtw_module.sql_generic_methods import *

medios_pago_file_path = "../../files/medios de pago.csv"
medios_pago_file = pd.read_csv(medios_pago_file_path)


def insert_medio_de_pago():
    # build the query and params
    query = "INSERT INTO medio_pago values (%(id_medio_pago)s, %(medio_pago)s, %(detalles)s);"

    for index, row in medios_pago_file.iterrows():
        values = {'id_medio_pago': index + 1, 'medio_pago': row["Medio de pago"], 'detalles': row["Detalles"]}
        try:
            generic_insert_whit_dict(query, values)
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)


# insert_medio_de_pago()
