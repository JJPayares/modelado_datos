import psycopg2

from db_config.config import config


def get_tables_from_schema(db_name):
    """Gets all tables names from the given db"""
    query = "SELECT table_name FROM information_schema.tables " \
            "WHERE table_catalog = %s AND table_schema = 'public';"

    connection = None
    table_names = None

    try:
        # read database configuration
        params = config()
        # connect to the PostgreSQL database
        connection = psycopg2.connect(**params)
        # create a new cursor
        cur = connection.cursor()
        # execute the SELECT statement
        cur.execute(query, (db_name,))
        # save the table names
        table_names = cur.fetchall()
        # commit the changes to the database
        connection.commit()
        # close communication with the database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if connection is not None:
            connection.close()

    return table_names


def generic_insert(sql_query, value):
    """
    Implements a simple insert by the given sql_query, use a single value.
    """
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute(sql_query, (value,))
        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def generic_insert_whit_dict(sql_query, _dict):
    """
    Implements a simple insert by the given sql_query, use a single value.
    """
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute(sql_query, _dict)
        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def get_id_of_municipio_by_name(name):
    """
    Returns the id of the given "ciudad/municipio"
    """

    query = "SELECT id_dpto_estado FROM dpto_estado WHERE dpto_estado.nombre_depto_estado = %s;"
    conn = None
    city_id = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (name,))
        city_id = cur.fetchone()[0]
        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

    return city_id


def get_field_by_param_and_table(field, param_field, param, table):
    """
    Returns only one record for the field of the given param and table
    """

    query = "SELECT {field} FROM {table} WHERE {table}.{param_field} = %s;".format(
        field=field, table=table, param_field=param_field
    )

    conn = None
    result = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (param,))
        result = cur.fetchone[0]
        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

    return result


def get_results_by_param_and_table(field, param_field, param, table, condition):
    """
    Returns the results of the query by the field of the given param and table
    """

    query = "SELECT {field} FROM {table} WHERE {table}.{param_field} {condition} %s;".format(
        field=field, table=table, param_field=param_field, condition=condition
    )

    conn = None
    result = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (param,))
        result = cur.fetchall()
        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

    return result


def execute_query(query):
    """executes the given query"""
    connection = None
    result = None
    try:
        # read database configuration
        params = config()
        # connect to the PostgreSQL database
        connection = psycopg2.connect(**params)
        # create a new cursor
        cur = connection.cursor()
        # execute the query statement
        cur.execute(query)
        # save the result
        result = cur.fetchall()
        # commit the changes to the database
        connection.commit()
        # close communication with the database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if connection is not None:
            connection.close()

    return result


"""sample = "SELECT * FROM producto WHERE producto.id_producto <> 0 AND producto.producto <> 'Mango Tommy Pintón' "
print(exect_query(sample))"""
