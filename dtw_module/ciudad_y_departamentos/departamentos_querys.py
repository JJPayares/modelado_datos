import pandas as pd

from dtw_module.sql_generic_methods import *
from utils.csv_utils import *

departamentos_municipios_file_path = "../../files/Departamentos_y_municipios_de_Colombia.csv"
file = pd.read_csv(departamentos_municipios_file_path)


def insert_departamentos_from_file():
    # extract unique values from colum DEPARTAMENTO
    lista_departamentos = get_unique_values_from_column(file, "DEPARTAMENTO")
    # build the query and params
    query = "INSERT INTO dpto_estado values %s;"

    for index, element in enumerate(lista_departamentos):
        value = (index + 1, element)
        try:
            generic_insert(query, value)
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)


def insert_ciudad_por_departamento_from_file():
    # relate each MUNICIPIO with his DEPARTAMENTO
    departamento_ciudad = make_pairs_of_data(file, 'DEPARTAMENTO', 'MUNICIPIO')
    # build the query and params
    query = "INSERT INTO ciudad values (%(id_ciudad)s, %(ciudad)s, %(id_dpto)s);"

    for index, element in enumerate(departamento_ciudad):
        # get the DEPARTAMENTO id for the insert
        id_dpto = get_id_of_municipio_by_name(element[0])
        values = {'id_ciudad': index + 1, 'ciudad': element[1], 'id_dpto': id_dpto}
        try:
            generic_insert_whit_dict(query, values)
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)

# insert_departamentos_from_file()
# print(insert_ciudad_por_departamento_from_file())
