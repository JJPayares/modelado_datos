import pandas as pd

from dtw_module.sql_generic_methods import *

from utils.basic_utils import *


def insert_ventas_productos():
    # build the query and params
    query = "INSERT INTO ventas_productos values (%(id_venta_producto)s, %(id_venta)s, %(id_producto)s);"

    ventas = get_first_element_of_element(
        get_results_by_param_and_table('id_venta', 'id_venta', 0, 'ventas', '<>')
    )
    productos = get_first_element_of_element(
        get_results_by_param_and_table('id_producto', 'id_producto', 0, 'producto', '<>')
    )
    for index, row in enumerate(ventas):
        venta = random.choice(ventas)
        producto = random.choice(productos)
        values = {'id_venta_producto': index + 1, 'id_venta': venta, 'id_producto': producto}
        try:
            generic_insert_whit_dict(query, values)
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)


insert_ventas_productos()
