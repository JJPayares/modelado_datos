from dtw_module.sql_generic_methods import *

ventas_por_dpto_sucursal = """
    SELECT 
        dpto.nombre_depto_estado AS DEPARTAMENTO
        , ct.nombre_ciudad AS CIUDAD
        , ssal.nombre_sucursal AS SUCURSAL
        , SUM(vtas.sub_total) AS SUB_TOTAL
        
        FROM 
            ciudad AS ct INNER JOIN dpto_estado AS dpto ON ct.id_dpto_estado = dpto.id_dpto_estado
            INNER JOIN sucursal AS ssal ON ssal.id_ciudad = ct.id_ciudad
            INNER JOIN empleado AS empl ON ssal.id_sucursal = empl.id_sucursal
            INNER JOIN ventas AS vtas ON vtas.id_vendedor = empl.id_empleado
            
            WHERE dpto.nombre_depto_estado = 'Antioquia' AND vtas.fecha BETWEEN '2022-01-01' AND '2022-06-01'
            
            GROUP BY  DEPARTAMENTO, CIUDAD, SUCURSAL
            ORDER BY SUB_TOTAL DESC
            """

ventas_vendedor_por_sucursal = """
    SELECT 
        dpto.nombre_depto_estado AS DEPARTAMENTO
        , ct.nombre_ciudad AS CIUDAD
        , ssal.nombre_sucursal AS SUCURSAL
        , empl.nombre || ' ' || empl.apellido AS VENDEDOR
        , SUM(vtas.sub_total) AS SUB_TOTAL
        
        FROM 
            ciudad AS ct INNER JOIN dpto_estado AS dpto ON ct.id_dpto_estado = dpto.id_dpto_estado
            INNER JOIN sucursal AS ssal ON ssal.id_ciudad = ct.id_ciudad
            INNER JOIN empleado AS empl ON ssal.id_sucursal = empl.id_sucursal
            INNER JOIN ventas AS vtas ON vtas.id_vendedor = empl.id_empleado
            
            WHERE ssal.nombre_sucursal = 'Voomm' AND vtas.fecha BETWEEN '2022-01-01' AND '2022-06-01'
            
            GROUP BY  DEPARTAMENTO, CIUDAD, VENDEDOR, SUCURSAL
            ORDER BY SUB_TOTAL DESC
"""

productos_por_sucursal_proveedor = """
    SELECT 
        dpto.nombre_depto_estado AS DEPARTAMENTO
        , ct.nombre_ciudad AS CIUDAD
        , ssal.nombre_sucursal AS SUCURSAL
        , prod.producto AS PRODUCTO
        , COUNT(PRODUCTO) AS CANTIDAD
            , SUM(vtas.sub_total) ACUMULADO
            , prov.nombre AS PROVEEDOR
            
            FROM 
                ciudad AS ct INNER JOIN dpto_estado AS dpto ON ct.id_dpto_estado = dpto.id_dpto_estado
                INNER JOIN sucursal AS ssal ON ssal.id_ciudad = ct.id_ciudad
                INNER JOIN empleado AS empl ON ssal.id_sucursal = empl.id_sucursal
                INNER JOIN ventas AS vtas ON vtas.id_vendedor = empl.id_empleado
                INNER JOIN ventas_productos AS vtpds ON vtpds.id_venta = vtas.id_venta
                INNER JOIN producto AS prod ON vtpds.id_producto = prod.id_producto
                INNER JOIN proveedor AS prov ON prov.id_proveedor = prod.id_proveedor
                
                WHERE ssal.nombre_sucursal = 'Voomm' 
                
                GROUP BY DEPARTAMENTO, CIUDAD, SUCURSAL, PRODUCTO, PROVEEDOR
                
                ORDER BY ACUMULADO DESC	
"""

ventas_prov_acc_sucursal = """
    SELECT 
        dpto.nombre_depto_estado AS DEPARTAMENTO
        , ct.nombre_ciudad AS CIUDAD
        , ssal.nombre_sucursal AS SUCURSAL
        , prov.nombre AS PROVEEDOR
        , SUM(vtas.sub_total) AS SUB_TOTAL_ACC
        
        FROM 
            ciudad AS ct INNER JOIN dpto_estado AS dpto ON ct.id_dpto_estado = dpto.id_dpto_estado
            INNER JOIN sucursal AS ssal ON ssal.id_ciudad = ct.id_ciudad
            INNER JOIN empleado AS empl ON ssal.id_sucursal = empl.id_sucursal
            INNER JOIN ventas AS vtas ON vtas.id_vendedor = empl.id_empleado
            INNER JOIN ventas_productos AS vtpds ON vtpds.id_venta = vtas.id_venta
            INNER JOIN producto AS prod ON vtpds.id_producto = prod.id_producto
            INNER JOIN proveedor AS prov ON prov.id_proveedor = prod.id_proveedor
            
            WHERE ssal.nombre_sucursal = 'Voomm' AND vtas.fecha BETWEEN '2022-01-01' AND '2022-06-01'
            
            GROUP BY DEPARTAMENTO, CIUDAD, SUCURSAL, PROVEEDOR
            
            ORDER BY SUB_TOTAL_ACC DESC
"""

productos_mas_vendidos_trimestre = """
    SELECT 
        prod.producto AS PRODUCTO
        , COUNT(PRODUCTO) AS CANTIDAD
        , prov.nombre AS PROVEEDOR
        
        FROM 
            ventas AS vtas INNER JOIN ventas_productos AS vtpds ON vtpds.id_venta = vtas.id_venta
            INNER JOIN producto AS prod ON vtpds.id_producto = prod.id_producto
            INNER JOIN proveedor AS prov ON prov.id_proveedor = prod.id_proveedor
            
                WHERE vtas.fecha BETWEEN '2022-01-01' AND '2022-03-31'
                
                GROUP BY PRODUCTO, PROVEEDOR
                
                ORDER BY CANTIDAD DESC
"""

frecuencia_medios_de_pago = """
    SELECT 
        mdpg.detalles AS MEDIO_DE_PAGO
        , COUNT(vtas.id_medio_pago) AS CANTIDAD
            , SUM(vtas.sub_total) AS ACUMULADO_VENTAS
            
            FROM 
                ventas AS vtas INNER JOIN medio_pago AS mdpg ON vtas.id_medio_pago = mdpg.id_medio_pago
                
                WHERE vtas.fecha BETWEEN '2022-01-01' AND '2022-03-31'
                
                GROUP BY MEDIO_DE_PAGO
                
                ORDER BY ACUMULADO_VENTAS DESC
"""

vendedores_sucursal_promedio_ventas = """
SELECT 
        suc.nombre_sucursal AS SUCURSAL
        , ciu.nombre_ciudad AS CIUDAD
        , COUNT(empl.id_empleado) AS NUM_EMPLEADOS
        , SUM(vtas.sub_total) AS ACUMULADO_VENTAS
        , to_char(AVG(vtas.sub_total),
            '99999999999999999D99' ) AS PROMEDIO
        
            FROM 
            sucursal AS suc INNER JOIN ciudad AS ciu ON suc.id_ciudad = ciu.id_ciudad
            INNER JOIN empleado AS empl ON empl.id_sucursal = suc.id_sucursal
            INNER JOIN ventas as vtas ON vtas.id_vendedor = empl.id_empleado
            
            WHERE suc.nombre_sucursal <> 'TEST'
            
            GROUP BY SUCURSAL, CIUDAD
"""

tiempo_promedio_prod_stock_prov = """
        SELECT 
        prod.producto AS PRODUCTO
        , prod.fecha_ingreso AS FECHA_INGRESO
        , vtas.fecha AS FECHA_VENTA
        , (vtas.fecha-prod.fecha_ingreso) AS TIEMPO_EN_STOCK
        , to_char(
            AVG(vtas.fecha-prod.fecha_ingreso), '99999999999999999D99' 
        ) AS TIEMPO_PROMEDIO_STOCK_DIAS
        , prov.nombre AS PROVEEDOR
        
        FROM 
            producto AS prod INNER JOIN ventas_productos AS vprods ON vprods.id_producto = prod.id_producto
            INNER JOIN ventas as vtas ON vprods.id_venta = vtas.id_venta
            INNER JOIN proveedor AS prov ON prod.id_proveedor = prov.id_proveedor
        
        WHERE (vtas.fecha-prod.fecha_ingreso) > 0
        
        GROUP BY PRODUCTO, FECHA_INGRESO, FECHA_VENTA, PROVEEDOR
        
        ORDER BY TIEMPO_EN_STOCK DESC
"""


print(execute_query(ventas_vendedor_por_sucursal))
