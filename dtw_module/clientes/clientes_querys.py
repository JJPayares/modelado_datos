import random

import pandas as pd

from dtw_module.sql_generic_methods import *

clientes_file_path = "../../files/Clientes.csv"
clientes_file = pd.read_csv(clientes_file_path)


def insert_clientes():
    # build the query and params
    query = "INSERT INTO cliente values " \
            "(%(id_cliente)s, %(nombre)s, %(apellido)s, %(edad)s, %(id_tipo_doc)s, %(email)s);"

    for index, row in clientes_file.iterrows():
        edad = str(random.randint(18, 87))
        values = {'id_cliente': index + 1, 'nombre': row["Nombre"], 'apellido': row["Apellido"], 'edad': edad,
                  'id_tipo_doc': row["Tipo Documento"], 'email': row["Email"]}
        try:
            generic_insert_whit_dict(query, values)
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)


# insert_clientes()
