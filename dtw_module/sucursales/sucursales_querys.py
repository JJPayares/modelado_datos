import pandas as pd

from dtw_module.sql_generic_methods import *

sucursales_file_path = "../../files/sucursales.csv"
sucursales_file = pd.read_csv(sucursales_file_path)


def insert_sucursales():
    # build the query and params
    query = "INSERT INTO sucursal values (%(id_sucursal)s, %(sucursal)s, %(id_ciudad)s);"

    for index, row in sucursales_file.iterrows():
        values = {'id_sucursal': index + 1, 'sucursal': row["Nombre"], 'id_ciudad': row["id_ciudad"]}
        try:
            generic_insert_whit_dict(query, values)
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)


# insert_sucursales()
