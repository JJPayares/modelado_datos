import pandas as pd

from dtw_module.sql_generic_methods import *

tproveedor_file_path = "../../files/Tipos de proveedor.csv"
tproveedor_file = pd.read_csv(tproveedor_file_path)

proveedor_file_path = "../../files/Proveedores.csv"
proveedor_file = pd.read_csv(proveedor_file_path)


def insert_tipos_de_proveedor():
    #  build the query and params
    query = "INSERT INTO tipo_proveedor values (%(id_tipo_proveedor)s, %(tipo_proveedor)s, %(detalles)s);"

    for index, row in tproveedor_file.iterrows():
        values = {'id_tipo_proveedor': index + 1, 'tipo_proveedor': row["Ubicacion"], 'detalles': row["Detalles"]}
        try:
            generic_insert_whit_dict(query, values)
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)


def insert_proveedores():
    #  build the query and params
    query = "INSERT INTO proveedor values (%(id_proveedor)s, %(nombre)s, %(nit)s, %(id_ciudad)s, %(telefono)s," \
            " %(email)s, %(id_tipo_proveedor)s);"

    for index, row in proveedor_file.iterrows():
        id_tproveedor = get_field_by_param_and_table("id_tipo_proveedor", "tipo_proveedor", row["Tipo Proveedor"],
                                                     "tipo_proveedor")
        values = {'id_proveedor': index + 1, 'nombre': row["Nombre"], 'nit': row["NIT"], 'id_ciudad': row["Ciudad"],
                  'telefono': row["Telefono"], 'email': row["Email"], 'id_tipo_proveedor': id_tproveedor}
        try:
            generic_insert_whit_dict(query, values)
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)


insert_tipos_de_proveedor()
# insert_proveedores()
