import pandas as pd

from dtw_module.sql_generic_methods import *
from utils.basic_utils import *

data_file_path = "../../files/datos/FU ORDER DETAIL - testcube.csv"
data_file = pd.read_csv(data_file_path)


def insert_tipo_producto():
    # build the query and params
    query = "INSERT INTO tipo_producto values (%(id_tipo_producto)s, %(tipo_producto)s, %(detalles)s);"
    # filter document by category and get unique values
    category = data_file['prod_category'].unique()

    for index, element in enumerate(category):
        values = {'id_tipo_producto': index + 1, 'tipo_producto': element, 'detalles': "Sin asignar"}
        try:
            generic_insert_whit_dict(query, values)
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)


# insert_tipo_producto()


def insert_producto():
    query = "INSERT INTO producto values (%(id_producto)s, %(id_tipo_producto)s, %(fecha_ingreso)s, " \
            "%(id_proveedor)s, %(producto)s);"
    # filter document by category and get unique values
    product = data_file[['prod_category', 'product_name']]
    for index, row in product.iterrows():
        id_category = get_field_by_param_and_table('id_tipo_producto', 'tipo_producto',
                                                   row['prod_category'], 'tipo_producto')
        fecha = random_date()
        id_proveedor = random.randint(1, 100)
        values = {'id_producto': index + 1, 'id_tipo_producto': id_category, 'fecha_ingreso': fecha,
                  'id_proveedor': id_proveedor, 'producto': row['product_name']}
        try:
            generic_insert_whit_dict(query, values)
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)


insert_producto()
