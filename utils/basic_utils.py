import random


def get_first_element_of_element(_list):
    output = []
    for element in _list:
        output.append(element[0])
    return output


def remove_single_quotes(string):
    if "'" in string:
        string.replace("'", "")
    return string


def random_date():
    day = random.randint(1, 30)
    month = random.randint(1, 12)
    year = random.randint(22, 22)
    return f'{day}-{month}-{year}'


def random_int(start, end):
    return random.randint(start, end)
