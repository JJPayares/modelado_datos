def get_unique_values_from_column(csv, column):
    values = csv[column].unique()
    return values


def make_pairs_of_data(file, group_1, group_2):
    """
    returns a list of tuples for group_1-group_2
    """
    group = file[[group_1, group_2]]
    group.reset_index()

    parejas = []
    for index, row in group.iterrows():
        line = row[group_1], row[group_2]
        parejas.append(line)

    return parejas
